// Header.js
import React from 'react';
import './Header.css';

const Header = () => {
  return (
    <header>
      <h1>React Demo Site</h1>
    </header>
  );
};

export default Header;
