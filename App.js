// App.js ggez
import React, { useState } from "react";
import Header from "./Header";
import Footer from "./Footer";
import BlueButton from "./BlueButton";
import GrayButton from "./GrayButton";
import "./container.css";
import "./contentBox.css";
import "./blueButton.css";
import "./grayButton.css";

const App = () => {
  const [number, setNumber] = useState(0);

  const isPrime = (num) => {
    if (num <= 1) {
      return false;
    }

    for (let i = 2; i <= Math.sqrt(num); i++) {
      if (num % i === 0) {
        return false;
      }
    }

    return true;
  };

  const incrementNumber = () => {
    setNumber(number + 1);
  };

  const decrementNumber = () => {
    setNumber(number - 1);
  };

  return (
    <div className="container">
      <Header />
      <div className="content-box">
        <center>
          <p>{number}</p>
          <BlueButton onClick={incrementNumber} />
          <GrayButton onClick={decrementNumber} />
          <p>
            {number}
            {isPrime(number) ? " เป็นจํานวนเฉพาะ" : " ไม่เป็นจํานวนเฉพาะ"}
          </p>
          {/* Add your main content here */}
        </center>
      </div>
      <Footer />
    </div>
  );
};

export default App;
