// BlueButton.js
import React from "react";

const BlueButton = ({ onClick }) => {
  return (
    <button className="blue-button" onClick={onClick}>
      เพิ่ม
    </button>
  );
};

export default BlueButton;
