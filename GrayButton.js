// GrayButton.js
import React from "react";

const GrayButton = ({ onClick }) => {
  return (
    <button className="gray-button" onClick={onClick}>
      ลด
    </button>
  );
};

export default GrayButton;
