// Footer.js
import React from "react";
import './Footer.css';

const Footer = () => {
  return (
    <footer>
      <p>Copyright © 2023 Wanrada Sutta</p>
    </footer>
  );
};

export default Footer;
